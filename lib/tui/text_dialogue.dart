import 'dart:io';

import 'package:easy_tui/cli/io/out/terminal_out.dart';

/// Abstract class responsible for showing _simple_ dialogues.
abstract class TextDialogue
{
	/*
	 * FIELDS
	 */

/// What a user should enter to indicate that they do not wish for something
	static const String CHOICE_NO  = "n";
/// What a user should enter to indicate that they do wish for something
	static const String CHOICE_YES = "y";
/// Guides the user towards correct input
	static const String INVALID    = 'You must answer "$CHOICE_YES" or "$CHOICE_NO"';
	static const String RESPONSE   = "\n>> Your response: ";
	static const String PROMPT     = "\n----------------------------------------------$RESPONSE";

	/*
	 * OPERATIONS
	 */

/// Ask a yes/no [question] to the user. 
///
/// Return `true` if yes, and `false` if no.
/// 
/// The acceptable responses to [question] are determined 
/// by [CHOICE_YES] and [CHOICE_NO].
	static bool yes( String question )
	{
		print("$question ($CHOICE_YES/$CHOICE_NO)");

///		[failures] tracks failed answer attempts
/// 
///		If the user fails too many times, the screen is cleared 
///		to prevent cluttering
		int failures = 0;

///		A `return` exits the `while` when acceptable input is entered
		while ( true )
		{
			print(PROMPT);
			final String input = stdin.readLineSync();

			if ( input == TextDialogue.CHOICE_YES )
			{
				return true;
			}
			else if ( input == TextDialogue.CHOICE_NO )
			{
				return false;
			}
			else
			{
				++failures;
				print(INVALID);
//				Clear the screen so it doesn't get too cluttered 
				if ( failures > 4 )
				{
					failures = 0;
					TerminalOut.enterToClear();
				}
			}
		}
	}
}
