import 'dart:io';

/// Aids with execution blocking so the user can process on-screen information.
abstract class TerminalOut
{
/// An ANSI code that clears the terminal.
	static const String ANSI_CLS = "\u001b[2J";
///	String that directs user on how to resume program execution.
	static const String WAIT = "Press ENTER to continue...";

/// __Pause execution__ until the __user presses enter__.
/// 
/// If [output] was passed, it is printed to the screen.
	static void enterToContinue([Object output]) 
	{
		if (output != null) 
		{
			print("\n${output.toString()}");
		}
		print("\n$WAIT");
		stdin.readLineSync();
	}

/// __Clear the terminal__ of all text.
/// 
/// Uses the ANSI code [ANSI_CLS] to clear it.
	static void clear() => print(ANSI_CLS);

/// __Pause execution__ until the __user presses enter__, and then __clear the screen__.
/// 
/// If [output] was passed, it is printed to the screen.
	static void enterToClear([Object output])
	{
		enterToContinue(output);
		clear();
	}
}
