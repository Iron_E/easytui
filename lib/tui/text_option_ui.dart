import 'dart:io';

import 'package:easy_tui/cli/io/in/terminal_input.dart';
import 'package:easy_tui/cli/io/out/terminal_out.dart';
import 'package:easy_tui/tui/layouts.dart';
import 'package:easy_tui/tui/menu_modifiers.dart';
import 'package:easy_tui/tui/text_dialogue.dart';
import 'package:easy_tui/tui/text_ui.dart';

/// Text-based UI allowing users to pick from a set of discrete options.
///
/// Data validation is handled by the [present], so the programmer knows
/// that information returned is either an indication of break ([BREAK_REPRESENTATION])
/// or a valid response.
class TextOptionUI extends TextUI<int>
{
	/*
	 * FIELDS
	 */

///	Indication that the user wishes to back out of the menu.
	static const int BREAK_REPRESENTATION = -2;
/// The preface used when none is specified in [present].
	static const String DEFAULT_PREFACE =
			"Enter a number corresponding to an action you would like to perform.";
	
	/*
	 * CONSTRUCTOR
	 */

	TextOptionUI( {Set<MenuModifiers> modifiers, Layouts layout =  Layouts.VERTICAL} ) :
		super(modifiers: modifiers, layout: layout);

	/*
	 * GETTERS
	 */

/// Determines if [value] is equal to [BREAK_REPRESENTATION].
	static bool isBreak( int value )
	{
		return value == TextOptionUI.BREAK_REPRESENTATION;
	}


///	Determines if [input] is valid in context with [lines].
/// 
/// Uses [lines.length] to determine if the user has input a number within
/// the number of options that the developer has specified.
/// 
/// E.g. If [lines.length] == `6`, then `6` is valid and so is `3`, but `0` and `7` are not.
	bool _inputIsWithinBounds( int input )
	{
		return (input <= this.lines.length) && (input > 0);
	}

	/*
	 * OPERATIONS
	 */

/// Prompt the user for a choice based on discrete options.
/// 
/// Shows the user a series of predefined options, and loops until they enter 
/// an acceptable input from [lines].
///
/// If present, [preface] acts as a foreword to the options.
///
/// If present, [prepend] is put in front of every `String` in [lines] 
/// before presentation to the user
	@override
	int present( {String preface = TextOptionUI.DEFAULT_PREFACE, String prepend} )
	{
		int    selectedOptionConverted;
		String selectedOption           = "";
		
		final String writeString = "$preface\n${this.toString(prepend)}" 
				"${this.promptModifierAdjustments()}"
				"${TextDialogue.PROMPT}";

		while ( true )
		{
			TerminalOut.clear();
			try
			{
				print(writeString);

				selectedOption = this.usesModifier(MenuModifiers.MAIN_MENU)
						? TerminalInput.readEvalExit(TextUI.EXIT) : stdin.readLineSync();

				selectedOptionConverted = int.parse(selectedOption);

				if ( this._inputIsWithinBounds(selectedOptionConverted) )
				{
					return selectedOptionConverted;
				} else
				{
					throw new FormatException();
				}
			}
			on FormatException
			{
				if ( !this.usesModifier(MenuModifiers.NO_EXIT) && (selectedOption == TextUI.EXIT) )
				{
					return BREAK_REPRESENTATION;
				} else
				{
					print("Please enter a valid number between 1 and ${this.lines.length}");
					TerminalOut.enterToClear();
				}
			}
		}
	}

	/*
	 * to.../from...
	 */

/// Writes all `Strings` from [lines] to a single `String` for [present]ation.
	@override
	String toString( [String prepend] )
	{
//		Much funnier in java...
		final StringBuffer bob = new StringBuffer();

		final String spaceChar = (this.layout == Layouts.VERTICAL) ? "\n" : "      ";

		for ( final String str in this.toStringIterative(prepend: prepend) )
		{
			bob..write(str)..write(spaceChar);
		}

		return bob.toString();
	}

}
