/// Various modifiers that affect `TextUI`s.
enum MenuModifiers
{
///	The menu is registered as a _main menu_.
/// Backing out of the menu will `exit(0)` the program.
	MAIN_MENU, 
/// The menu is registered as essential, and thusly
/// backing out of it will not be permitted.
	NO_EXIT
}

