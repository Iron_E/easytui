# 3.X.Y

## 3.3.1

* Switch away from Creative Commons for licensing.
* Remove mention of project which no longer uses this library.

## 3.3.0

* Fix bug preventing `prepend` from functioning on `TextOptionUI`s

## 3.2.0

* Change the way that `preface` works in `TextFormUI`s
	* It functioned differently than `TextOptionUI`, and it makes more sense
		to have uniformity.

## 3.1.0

* Change some text to better reflect the usage of `easy_tui`

## 3.0.3

* Use `dartfmt` on project as per [pub.dev](https://pub.dev/packages/easy_tui#-analysis-tab-)'s suggestions

## 3.0.2

* Fix oversight in documentation during refactor

## 3.0.1

* Fix bug introduced by `3.0.0`
	* `List<MenuModifier>`s were not changed over to `Set<MenuModifier>`s

## 3.0.0

* Change data structure for containing modifiers in `TextUI`
* Even though this is a `MAJOR` path revision, the change is actually quite small.

# 2.X.Y

## 2.0.6

* Update documentation to reflect most recent versions

## 2.0.5

* Remove extraneous dashed lines from `TerminalDialogue.PROMPT` as introduced by `2.0.4`

## 2.0.4

* Make several things easier to read in the menus

## 2.0.3

* Fix number-bullets showing when they shouldn't
* Change order of yes/no error message

## 2.0.2

* Fix issue with not quitting `TextFormUI`s

## 2.0.1

* Improve documentation

## 2.0.0

* Change `reviseAnswers()` method signature to return `Map<String, String>` instead of two `List<String>`.
	* This change is in hope to solidify the relationship between the questions and the answers

# 1.X.Y

## 1.0.4

* Fix errors in `example/main.dart` created by the `1.0.3` update

## 1.0.3

* Update to follow "strict" mode
* Add more lint options
	* Subsequently update all files to follow linting rules

## 1.0.2

* Add file to import every class in one go, `lib/tui.dart`

## 1.0.1

* Ran `dartfmt` on all files as suggested by `pub`
* I will be doing this whenever I pull into this branch, however the master & etc. branches will remain formatted _my_ way

## 1.0.0

* Initial release
* See `example/` folder, dartdocs, or README to get a basic understanding of the library's function.
