import 'package:easy_tui/tui/layouts.dart';
import 'package:easy_tui/tui/menu_modifiers.dart';
import 'package:meta/meta.dart';


/// Base type for TextUI classes.
///
/// [T] is type that will be returned by [present].
abstract class TextUI<T>
{

	/*
	 * FIELDS
	 */

/// What the user should input when they wish to back out of a `TextUI`.
	static const String EXIT       = "q";
/// A [Set] of [MenuModifiers] that change how the [TextUI] behaves.
	Set<MenuModifiers> _modifiers;
/// The layout of the [TextUI]
	Layouts _layout;
	List<String> _lines;
	
	/*
	 * CONSTRUCTOR
	 */

	TextUI( {Set<MenuModifiers> modifiers, Layouts layout =  Layouts.VERTICAL} )
	{
		this._modifiers = modifiers;
		this._layout = layout;
		this._lines = new List<String>();
	}

	/*
	 * PROPERTIES
	 */

	Layouts get layout => this._layout;
	set layout( Layouts newLayout ) => this._layout = newLayout;

/// The [String]s of text that the [TextUI] should display
	List<String> get lines => this._lines;

	/*
	 * METHODS
	 */

/// Returns a [T] containing the user's inputs for each [line].
///
/// [preface] will give the user directions before data entry.
/// [prepend] will prepend each question/prompt before output to the user.
	T present( {String preface, String prepend} );

///	Determines whether or not the UI uses [modifier].
///
/// Does so by checking [_modifiers] for [modifier].
	bool usesModifier( MenuModifiers modifier )
	{
		return this._modifiers?.any((MenuModifiers opt) => opt == modifier) ?? false;
	}

/// Generate a string based on [MenuModifiers].
	@protected
	String promptModifierAdjustments()
	{
//		The joke is funnier in Java...
		final StringBuffer bob = new StringBuffer()
				..write('\n(enter a lone "$EXIT" to ');

		if ( this.usesModifier(MenuModifiers.MAIN_MENU) )
		{
			bob.write("quit the program");
		}
		else if ( !this.usesModifier(MenuModifiers.NO_EXIT) )
		{
			bob.write("exit this menu");
		}
		else
		{
			return "";
		}

		bob.write(")");
		return bob.toString();
	}

/// Format a new [List] of [String]s from [lines].
	@protected
	List<String> toStringIterative( { String prepend, bool numberBullets = true } )
	{
		final List<String> returnArray = new List<String>(this._lines.length);

		for ( int i = 0; i < this.lines.length; ++i )
		{
			String value = "";
			if ( numberBullets ) 
			{
				value += "${i + 1}. ";
			}
			value += "${prepend ?? ""}${this._lines[i]}"; 
			returnArray[i] = value;
		}

		return returnArray;
	}
}

