import 'dart:io';

import 'package:easy_tui/cli/io/out/terminal_out.dart';
import 'package:easy_tui/tui/menu_modifiers.dart';
import 'package:easy_tui/tui/text_form_ui.dart';
import 'package:easy_tui/tui/text_option_ui.dart';



void main()
{
//	Create the UI class 
	final TextOptionUI main = new TextOptionUI(
///			The [modifier] parameter is a list of modifiers
/// 		In this case, `MenuModifiers.MAIN_MENU` is the modifier.
			modifiers: <MenuModifiers>{ MenuModifiers.MAIN_MENU }
		);

/// Add content to a `TextUI` through the [lines] property
	main.lines.addAll(<String>[
		/* 1 */ "Say hello",
		/* 2 */ "Go to a submenu",
		/* 3 */ "Enter some data",
		/* 4 */ "Exit"
	]);


/// `TextUI`s with the `MenuModifiers.MAIN_MENU` property __should loop__
	while ( true )
	{
///		`switch` statements are a good way to handle [present]'s return value
		switch ( main.present() ) 
		{
			case 1:
				print("\nHello, user!\n");
				break;
			case 2:
				TerminalOut.enterToContinue(
						"I didn't feel like demoing this, " 
						"but you can put as many menus into menus as your feel like."
					);
				break;
			case 3:
				final TextFormUI dataEntry = new TextFormUI();
				const String queryName = "name?";
				const String queryAge  = "age?";
				const String queryDOB  = "date of birth?";
				dataEntry.lines.addAll(<String>[
					queryName,
					queryAge,
					queryDOB
				]);

///				[present()] has an  __optional named parameter__ 
///				that prepends the questions with the `String` provided.
/// 			
/// 			So, the questions would be "_What is your_ name?", 
/// 			"_What is your_ age?", and "_What is your_ date of birth"?
				final Map<String, String> answers = dataEntry.present(prepend: "What is your ");
				print(
						"\nYour name is: ${answers[queryName]}, "
						"\nYour age is: ${answers[queryAge]}, " 
						"\nYour DOB is: ${answers[queryDOB]}"
					);
				break;
			case 4:
				exit(0);
				break;
		}
		TerminalOut.enterToClear("Returning to main menu...");
	}
}
