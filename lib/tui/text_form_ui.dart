import 'dart:io';

import 'package:meta/meta.dart';

import 'package:easy_tui/cli/io/out/terminal_out.dart';
import 'package:easy_tui/tui/layouts.dart';
import 'package:easy_tui/tui/menu_modifiers.dart';
import 'package:easy_tui/tui/text_dialogue.dart';
import 'package:easy_tui/tui/text_option_ui.dart';
import 'package:easy_tui/tui/text_ui.dart';

/// A subtype of `TextUI` that accepts _variable_ input from a user.
///
/// __Data validation is up to the programmer__. No "valid" types are 
/// assumed by [present], and all answers return as a `String`.
/// 
/// However, [displayInvalidEntry] has been implemented to help 
/// guide users in the right direction.
class TextFormUI extends TextUI<Map<String, String>>
{
	/*
	 * FIELDS
	 */

	static const String _confirmExit = 
				'Is "${TextUI.EXIT}" your input ("${TextDialogue.CHOICE_YES}"), ' 
				'or do you wish to quit data entry ("${TextDialogue.CHOICE_NO}")?';

	/*
	 * CONSTRUCTOR
	 */
	
	TextFormUI( {Set<MenuModifiers> modifiers, Layouts layout =  Layouts.VERTICAL} ) :
		super(modifiers: modifiers, layout: layout);

	/*
	 * OPERATIONS
	 */

///	Displays a custom error resulting from [badAnswer], and clarifies the correct type [typeName].
	static void displayInvalidEntry( {@required String badAnswer, @required String typeName} )
	{
		TerminalOut.enterToClear(
				'One of your entries was invalid. See below:'
				'\n"$badAnswer" is not a valid $typeName'
			);
	}

/// Ask the user to enter input for a series of prompts specified in [this.lines].
	@override
	Map<String, String> present( {String preface, String prepend} )
	{
///		Labelled "frozen" because any concurrent changes are not reflected during [present()]
		final List<String> frozenQuestions = this.toStringIterative(prepend: prepend);
		final List<String> answers         = new List<String>(frozenQuestions.length);
		final String interfaceDirections   = this.promptModifierAdjustments();

		TerminalOut.clear();

		if ( preface == null )
		{
			preface = "";
		}
		else
		{
			preface += "\n";
		}

		for ( int i = 0; i < frozenQuestions.length; i++ )
		{
			print(
					"$preface"
					"${frozenQuestions[i]}"
					"\n$interfaceDirections${TextDialogue.PROMPT}"
				);
			final String input = stdin.readLineSync();

///			`true` if the user signifies that they wish to leave 
			if ( (input == TextUI.EXIT) && (!TextDialogue.yes(_confirmExit)) )
			{
				return null;				
			}
			else
			{
				answers[i] = input;
			}
			TerminalOut.clear();
		}
		TerminalOut.enterToClear();

//		Give the user a chance to correct any mistakes that they may have made
		return TextFormUI.reviseAnswers(
				Map<String, String>.fromIterables(
//						The keys
						this.toStringIterative(
//								Regenerate the questions without the number-bullets
//								This is to prevent a number-bullet duplication bug
								prepend: prepend, numberBullets: false
							), 
//						The values
						answers
					)
			);
	}

/// Prompt user with the keys of [prefacedData] and let them change its values.
///
/// The keys of [prefacedData] are like explanations, or questions that explain 
/// what the data ([prefacedData]'s values) are.
/// 
/// * For example, a valid map might be
/// ```dart 
/// <String, String>{'Your current favorite number':'42'};
/// ```
/// ...where "42" is that somebody's current favorite number.
///		* In this example, 42 is also the number that will be editable by [reviseAnswers].
	static Map<String, String> reviseAnswers( Map<String, String> prefacedData )
	{
		const String continueChanging = "Would you like to change any of them?";

		final List<MapEntry<String, String>> revisionEntries = 
				new List<MapEntry<String, String>>.of(prefacedData.entries);
		
		while ( true )
		{
			final TextOptionUI selectData = new TextOptionUI(
					modifiers: <MenuModifiers>{ MenuModifiers.NO_EXIT }
				);
			print("Your answers:\n=========================");
			for ( int i = 0; i < revisionEntries.length; ++i )
			{
				selectData.lines.add(
///						[TextDialogue.PROMPT] has a "\n", so no "\n" is needed here
						"${revisionEntries[i].key}"
						"${TextDialogue.RESPONSE}${revisionEntries[i].value}\n"
					);
				print(selectData.lines.last);
			}

			if ( TextDialogue.yes(continueChanging) )
			{
///				[present()] is a blocking call
				final int revisionIndex = selectData.present( 
						preface: "Enter the number of which you would like to change."
					) - 1;

				TerminalOut.clear();

				print(
						"${revisionEntries[revisionIndex].key}"
//						Give the previous answer, for reference
						"\n– Previous answer: ${revisionEntries[revisionIndex].value}" 
						"${TextDialogue.PROMPT}"
					);

				final String input = stdin.readLineSync();
				if ( input != "" ) 
				{
//					Since `MapEntry.value` is final, we have to create a new object
					revisionEntries[revisionIndex] = new MapEntry<String, String>(
							revisionEntries[revisionIndex].key, input
						);
				}
			}
			else
			{
				break;
			}
			TerminalOut.enterToClear();
		}

//		Update the original map, since it probably is more 
//		efficient than just creating a new one
		for ( final MapEntry<String, String> e in revisionEntries )
		{
			prefacedData[e.key] = e.value;
		}

		return prefacedData;
	}
}
