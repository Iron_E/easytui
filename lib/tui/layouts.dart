/// Determines a `TextUI`'s orientation.
enum Layouts
{
/// The UI will be displayed ← _horizontally_ →
	HORIZONTAL, 
/// The UI will be displayed ↑ _vertically_ ↓
	VERTICAL
}
