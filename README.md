# EasyTUI

## About

This is a small library for generating text-driven menus in the terminal.

In this library, **TUI** stands for **T**ext **U**ser **I**nterface

## Documentation

* Dartdoc [has been used](https://pub.dev/documentation/easy_tui/latest/) document this library and its use.

### Examples

See the `examples/` folder.
