import 'dart:io';

/// A wrapper around `dart:io` to allow checking for exit conditions
abstract class TerminalInput
{

///	Determine whether to exit the program by executing [checkExit] on user input.
/// 
///	This function accepts a method [checkExit] which checks 
///	whether or not a user's input indicates that the program should terminate.
	static String readCheckExit( bool checkExit(String input) )
	{
		final String userInput = stdin.readLineSync();

		if ( checkExit(userInput) )
		{
			exit(0);
		}
		return userInput;
	}

/// Compare [userInput] with [exitCondition] to check if the program should terminate.
/// 
/// If [userInput] _does_ match [exitCondition], then the program exits.
/// Otherwise, the value of [userInput] is returned.
	static String readEvalExit( dynamic exitCondition )
	{
		final String userInput = stdin.readLineSync();

		if ( userInput == exitCondition?.toString() )
		{
			exit(0);
		}
		return userInput;
	}
}
